const Painting = require('../src/models/Painting.js');
const Round = require('../src/models/Round.js');

describe('Testing Round functions', () => {
    test('Rounding a number to two decimal places', async() => {
        expect(Round(2.667389543, 2)).toBe(2.67);
    })
    test('Rounding a number to an integer', async() => {
        expect(Round(2.667389543)).toBe(3);
    })
})


describe('Testing Painting functions', () => {
    test('Calculate the paint that will be used', () => {
        expect(Painting.calculatePaintThatBeUsed(100)).toBe(20)
    })

    test('Calculate total area', async() => {
        const walls = [{
            "height": 3,
            "width": 8,
            "doors": 1,
            "windows": 0
        }, {
            "height": 5,
            "width": 2,
            "doors": 0,
            "windows": 1
        }, {
            "height": 5,
            "width": 8,
            "doors": 1,
            "windows": 0
        }, {
            "height": 5,
            "width": 8,
            "doors": 1,
            "windows": 0
        }];
        expect(Painting.calculateTotalArea(walls)).toBe(107.04);
    })

    test('Calculate total cans of paint', () => {
        const result = [{ "capacity": 18, "theAmount": 1 }, { "capacity": 3.6, "theAmount": 0 }, { "capacity": 2.5, "theAmount": 0 }, { "capacity": 0.5, "theAmount": 4 }]
        expect(Painting.calculateTotalCans(100)).toStrictEqual(result)
    })

    test('Calculate the amount of paint can', () => {
        const result = { "amountOfPaintCans": 5, "can": 20, "remainingLiters": 0 };
        expect(Painting.calculateCanQuantity(100, 20)).toStrictEqual(result)
    })

})