FROM node:alpine

WORKDIR /digitalrepublic-code-challenge

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]