# digitalrepublic-code-challenge

# Sumário

- [Tecnologias usadas](#tecnologias-usadas)
- [Configuração inicial](#configuração-inicial)
- [Executando a aplicação](#executando-a-aplicação)
- [Testes](#testes)
- [Rotas](#rotas)
   	- [POST `/painting`](#post-painting)

## Tecnologias usadas
> Desenvolvido usando Javascript, Node.js, Express, Jest e Docker.

## Configuração Inicial

Assim que você clonar o repositório para sua máquina, execute o seguinte comando no terminal, dentro da pasta do projeto, para que a imagem Docker do projeto seja criada.
```
docker build -t digitalrepublic/code-challenge .
```
Utilizando o comando abaixo no terminal, você irá executar o projeto:
```
docker run -p 3000:3000 -it --rm digitalrepublic/code-challenge /bin/ash
```
Após estes 2 passos terem sido concluidos, você pode escolher se irá rodar o projeto, ou executar os testes, caso tenha alguma dúvida, apenas consulte os proximos tópicos.


## Executando a aplicação

Para executar a aplicação normalmente:
```bash
npm start
```
Para executar em modo de desenvolvimento, com a ferramenta `Nodemon`, onde a cada vez que um arquivo é alterado, o servidor é reiniciado automaticamente, assim, não há a necessidade de utilizar o `npm start` a cada alteração feita nos arquivos.
```bash
npm run dev
```

## Testes
A API possui até o momento somente testes unitários para a camada Model, para rodar os testes basta executar o comando abaixo:
```
npm test
```

## Rotas
###  POST `/painting`
Rota responsável calcular a quantidade de tinta que será usada para pintar uma determinada região. O `body` da requisição deve ter o seguinte formato:

```json
{
    "walls": [{
            "height": 5,
            "width": 8,
            "doors": 0,
            "windows": 5
        },
        {
            "height": 5,
            "width": 2,
            "doors": 0,
            "windows": 1
        },
        {
            "height": 5,
            "width": 8,
            "doors": 1,
            "windows": 0
        },
        {
            "height": 5,
            "width": 8,
            "doors": 1,
            "windows": 0
        }
    ]
}
```
Exemplo de retorno com sucesso:
```json
{
    "totalArea": 112.56,
    "paintToBeUsed": 22.51,
    "paintCans": [
        {
            "capacity": 18,
            "theAmount": 1
        },
        {
            "capacity": 3.6,
            "theAmount": 1
        },
        {
            "capacity": 2.5,
            "theAmount": 0
        },
        {
            "capacity": 0.5,
            "theAmount": 2
        }
    ]
}
```

#### Regras:
- Nenhum dos atributos poderá estar vazio.
- O array da requisição foi pensado, para que cada posição no array representasse um lado da parede, assim cada array possui seus valores.
- A altura de paredes com porta deve ser, no mínimo, 2.10 metros.
- As medidas de portas e janelas já são predefinidas, é possivel somente escolher a quantidade delas.
---
