const express = require("express");
const router = express.Router();
const PaintingController = require("../controllers/paintingController");

router.post("/painting", PaintingController.painting);

module.exports = router;