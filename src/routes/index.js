const express = require("express");
const ink = require("./paintingRoutes.js");

let routes = (app) => {

    app.use(
        express.json(),
        ink
    )

    app.get("/", (req, res) => {
        res.status(200).send("Home Page");
    })

}

module.exports = routes;