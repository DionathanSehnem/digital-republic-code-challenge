module.exports = Round = (num, places) => {
    return +(parseFloat(num).toFixed(places));
}