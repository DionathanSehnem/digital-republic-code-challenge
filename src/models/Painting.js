const Round = require('./Round.js');

const paintCanSize = [0.5, 2.5, 3.6, 18];
const doorMeasure = 0.8 * 1.90;
const windowMeasure = 2 * 1.2;
const minimumWallHeight = 1.90 + 0.30;

class Painting {

    static calculatePaintThatBeUsed = (totalArea) => {
        return Round(totalArea / 5, 2);
    }

    static calculateTotalArea = (walls) => {
        let totalArea = 0;
        if (walls.length > 4 || walls.length < 4) {
            throw new Error('The wall must have exactly 4 sides, it cannot have less or more');
        }
        walls.forEach(wall => {
            const windowAndDoorArea = Round((wall.doors * doorMeasure) + (wall.windows * windowMeasure), 2);
            const wallArea = Round(wall.height * wall.width, 2);

            if (wallArea < 1 || wallArea >= 50) {
                throw new Error('No wall can be less than 1 square meter and no more than 50 square meters');
            }
            if (wall.doors > 0 && wall.height < minimumWallHeight) {
                throw new Error(`The height of walls with a door must be at least ${Round(minimumWallHeight, 4)} meters`);
            }
            if (windowAndDoorArea > wallArea / 2) {
                throw new Error('The total area of ​​the doors and windows must be a maximum of 50% of the wall area');
            }

            totalArea += wallArea - windowAndDoorArea;
        });
        return Round(totalArea, 2);
    }

    static calculateTotalCans = (area) => {
        let remainingLiters = this.calculatePaintThatBeUsed(area);
        let paintCans = [];

        paintCanSize.sort((a, b) => {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        }).reverse().forEach(can => {
            const data = this.calculateCanQuantity(remainingLiters, can);
            remainingLiters = data.remainingLiters;
            paintCans.push({
                capacity: can,
                theAmount: data.amountOfPaintCans
            })
        })
        return paintCans;
    }

    static calculateCanQuantity = (liters, can) => {
        let remainingLiters = Round(liters % can, 1);
        let amountOfPaintCans = Round((liters - remainingLiters) / can, 1);
        if (can == 0.5 && remainingLiters > 0) {
            amountOfPaintCans += 1;
            remainingLiters = Round(remainingLiters - can, 1);
        }
        return {
            can: can,
            remainingLiters: remainingLiters,
            amountOfPaintCans: amountOfPaintCans
        }
    }

}

module.exports = Painting;