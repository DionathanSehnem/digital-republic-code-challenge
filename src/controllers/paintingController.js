const Painting = require('../models/Painting.js');
const Round = require('../models/Round.js');

class PaintingController {

    static painting = (req, res) => {
        try {
            const { walls } = req.body;
            const totalArea = Painting.calculateTotalArea(walls);
            const result = {
                totalArea: totalArea,
                paintToBeUsed: Painting.calculatePaintThatBeUsed(totalArea),
                paintCans: Painting.calculateTotalCans(totalArea)
            }
            res.status(200).json(result)
        } catch (error) {
            res.status(400).send(`${error}`)
        }
    }
}

module.exports = PaintingController;